package com.example.coffeeapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val coffeeList = arrayListOf<Coffee>()

    val addCoffeeLambda: (Coffee) -> Unit = {
        coffeeList.add(it)
        Toast.makeText(this, "coffee # in Activity: ${coffeeList.size}", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottom_menu.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.coffee_home -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_global_coffeeFragment)
                    true
                }
                R.id.coffee_cart -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_global_cartFragment)
                    true
                }
                else -> false
            }
        }
    }
}