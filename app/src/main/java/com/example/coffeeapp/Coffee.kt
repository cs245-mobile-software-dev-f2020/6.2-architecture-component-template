package com.example.coffeeapp

data class Coffee(
    var size: String,
    var qty: Int
)