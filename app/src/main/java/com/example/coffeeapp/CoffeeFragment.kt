package com.example.coffeeapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_coffee.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CoffeeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CoffeeFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    val coffeeList = arrayListOf<Coffee>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        add_button.setOnClickListener {
            val size = when (coffee_radioGroup.checkedRadioButtonId) {
                small_radioButton.id -> "Small"
                medium_radioButton.id -> "Medium"
                large_radioButton.id -> "Large"
                else -> "Small"
            }
            val qty = coffee_qty_text.text.toString().toInt()
            coffeeList.add(Coffee(size, qty))
            Toast.makeText(activity, "coffee # in Fragment: ${coffeeList.size}", Toast.LENGTH_SHORT)
                .show()
//            clickLambda(Coffee(size, qty))
        }
    }

    lateinit var clickLambda: (Coffee) -> Unit
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            clickLambda = context.addCoffeeLambda
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_coffee, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CoffeeFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CoffeeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}